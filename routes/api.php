<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('getfeesettings', function (\App\Yearvotehead $yearvotehead, Request $request) {
    return response()->json($yearvotehead::with('votehead')->where('year', $request->year)->get()
    );
});

Route::post('feestructure','FeestructureController@store');
