<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth::loginUsingId(1);
Route::get('install', function () {
    Artisan::call('migrate:fresh');
    return response()->json("Done");
});
Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin'], function () {
    Route::resource('student', 'StudentController');
    Route::resource('house', 'HouseController');
    Route::resource('votehead', 'VoteheadController');
    Route::resource('term', 'TermController');
    Route::resource('form', 'FormController');
    Route::resource('year', 'YearController');
    Route::resource('feesettings', 'FeesettingsController');

    Route::group(['prefix' => 'settings'], function () {
        Route::resource('yearvoteheads', 'YearvoteheadController');
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
