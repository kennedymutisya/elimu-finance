<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Feesettings
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feesettings newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feesettings newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feesettings query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feesettings whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feesettings whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feesettings whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Feesettings extends Model
{
    //
}
