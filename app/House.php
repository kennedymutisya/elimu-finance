<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\House
 *
 * @property int $id
 * @property string $house
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\House newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\House newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\House query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\House whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\House whereHouse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\House whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\House whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class House extends Model
{
    //
}
