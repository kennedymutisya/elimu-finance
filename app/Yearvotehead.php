<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Yearvotehead
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Yearvotehead newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Yearvotehead newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Yearvotehead query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $year
 * @property int $votehead_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Yearvotehead whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Yearvotehead whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Yearvotehead whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Yearvotehead whereVoteheadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Yearvotehead whereYear($value)
 */
class Yearvotehead extends Model
{
    public function votehead()
    {
        return $this->hasOne(Votehead::class,'id','votehead_id');

    }

    public function year()
    {
        return $this->hasOne(Year::class, 'id', 'year');
    }
}
