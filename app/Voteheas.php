<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Voteheas
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voteheas newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voteheas newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voteheas query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voteheas whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voteheas whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voteheas whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Voteheas extends Model
{
    //
}
