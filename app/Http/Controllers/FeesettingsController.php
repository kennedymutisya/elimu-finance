<?php

namespace App\Http\Controllers;

use App\Feesettings;
use Illuminate\Http\Request;

class FeesettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.feesettings');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feesettings  $feesettings
     * @return \Illuminate\Http\Response
     */
    public function show(Feesettings $feesettings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Feesettings  $feesettings
     * @return \Illuminate\Http\Response
     */
    public function edit(Feesettings $feesettings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feesettings  $feesettings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feesettings $feesettings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feesettings  $feesettings
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feesettings $feesettings)
    {
        //
    }
}
