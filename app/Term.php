<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Term
 *
 * @property int $id
 * @property int $term
 * @property int $year
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereTerm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereYear($value)
 * @mixin \Eloquent
 */
class Term extends Model
{
    //
}
