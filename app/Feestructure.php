<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Feestructure
 *
 * @property int $id
 * @property int $year_id
 * @property int $form_id
 * @property int $votehead_id
 * @property int $termone
 * @property int $termtwo
 * @property int $termthree
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feestructure newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feestructure newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feestructure query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feestructure whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feestructure whereFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feestructure whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feestructure whereTermone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feestructure whereTermthree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feestructure whereTermtwo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feestructure whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feestructure whereVoteheadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Feestructure whereYearId($value)
 * @mixin \Eloquent
 */
class Feestructure extends Model
{
    //
}
