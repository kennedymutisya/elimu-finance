<nav class="side-navbar box-scroll sidebar-scroll">
    <!-- Begin Main Navigation -->
    <ul class="list-unstyled">
        <li><a href="javascript:void(0);">
                <i class="la la-at"></i>

                <span>Dashboard</span>
            </a>
        </li>
        <li>
            <a href="#dropdown" aria-expanded="false" data-toggle="collapse">
                <i class="fa fa-registered"></i>
                <span>Registration</span>
            </a>
            <ul id="dropdown" class="collapse list-unstyled pt-0">
                <li><a href="{{ route('student.index') }}">Students</a></li>
                <li><a href="{{ route('form.index') }}">Classes </a></li>
                <li><a href="{{ route('term.index') }}">Term</a></li>
                <li><a href="{{ route('house.index') }}">Houses</a></li>
                <li><a href="{{ route('votehead.index') }}">Voteheads</a></li>
                <li><a href="{{ route('year.index') }}">Year</a></li>
            </ul>
        </li>
        <li>
            <a href="#year" aria-expanded="false" data-toggle="collapse">
                <i class="fa fa-cogs"></i>
                <span>Settings</span>
            </a>
            <ul id="year" class="collapse list-unstyled pt-0">
                <li><a href="{{ route('yearvoteheads.index') }}">Year Voteheads</a></li>
                <li><a href="{{ route('feesettings.index') }}">Fee Settings </a></li>
                <li><a href="{{ route('term.index') }}">Invoice Students</a></li>
            </ul>
        </li>
    </ul>
    <ul class="list-unstyled">
        <li><a href="#"><i class="la la-angle-left"></i><span>Back To Elimu</span></a></li>
    </ul>
    <!-- End Main Navigation -->
</nav>