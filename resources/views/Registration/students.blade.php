@extends('layouts.app')

@section('content')
    <studentregistration
            :houses="{{ $house }}"
                         :term="{{ $term }}"
                         :form="{{$form}}"
    ></studentregistration>

    <div class="mt-5">
        <students :allstudents="{{ $student}}"></students>
    </div>
@endsection
