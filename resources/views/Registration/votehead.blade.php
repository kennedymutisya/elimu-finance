@extends('layouts.app')

@section('content')
    <votehead></votehead>
    <div class="m-10 p-5">
        <voteheadsall :votehead="{{ $voteheads }}"></voteheadsall>
    </div>
@endsection
