@extends('layouts.app')

@section('content')
    <houseregistration></houseregistration>
    <div class="mt-5">
        <houses :house="{{$house}}"></houses></div>
@endsection
