@extends('layouts.app')

@section('content')
    <term></term>
    <div class="m-10 p-5">
        <terms :years="{{ $years }}"></terms>
    </div>
@endsection
