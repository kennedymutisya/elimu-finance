/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
// window.dt = require('datatables');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('studentregistration', require('./components/Registration/student'));
Vue.component('houseregistration', require('./components/Registration/house'));
Vue.component('houses', require('./components/Registration/houses'));
Vue.component('votehead', require('./components/Registration/votehed'));
Vue.component('voteheadsall', require('./components/Registration/voteheads'));
Vue.component('term', require('./components/Registration/term'));
Vue.component('terms', require('./components/Registration/terms'));
Vue.component('formregistration', require('./components/Registration/form'));
Vue.component('forms', require('./components/Registration/formview'));
Vue.component('students', require('./components/Registration/studentsview'));
Vue.component('year', require('./components/Registration/year'));
Vue.component('years', require('./components/Registration/yearview'));
Vue.component('yearvoteheads', require('./components/settings/yearvoteheads'));
Vue.component('yearvoteheadsview', require('./components/settings/yearvoteheadsview'));
Vue.component('feesettings', require('./components/settings/feesettings'));

window.Bus = new Vue({});
const app = new Vue({
    el: '#app'
});
