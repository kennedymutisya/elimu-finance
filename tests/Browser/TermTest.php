<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TermTest extends DuskTestCase
{
    use RefreshDatabase;
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/admin/term')
                ->select('term', '1')
                ->type('year', 2018)
                ->press('Add Term')
                ->select('term', '2')
                ->type('year', 2018)
                ->press('Add Term')
                ->select('term', '3')
                ->type('year', 2018)
                ->press('Add Term')
            ->pause(5000);
        });
    }
}
