<?php

namespace Tests\Browser;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class VoteheadTest extends DuskTestCase
{
//    use RefreshDatabase;

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
//            $user = factory(User::class)->create();
            $browser
                ->maximize()
                ->loginAs(User::find(1))
//                ->pause(7000)
                ->visit('admin/votehead')
                ->type('voteheadid', 'TLM&E')
                ->type('votehead', 'Teaching learning materials and exams')
                ->press('Add Votehead')
//                ->pause(5000)
                ->type('voteheadid', 'BE&S')
                ->type('votehead', 'Boarding Equipment and Stores')
                ->press('Add Votehead')
                ->type('voteheadid', 'RM&I')
                ->type('votehead', 'Repairs, maintenance and improvement')
                ->press('Add Votehead')
                ->type('voteheadid', 'LT&T')
                ->type('votehead', 'Local travel and transport')
                ->press('Add Votehead')
                ->type('voteheadid', 'AC')
                ->type('votehead', 'Administration costs')
                ->press('Add Votehead')
                ->type('voteheadid', 'EWC')
                ->type('votehead', 'Electricity, Water and conservancy')
                ->press('Add Votehead')
                ->type('voteheadid', 'AF')
                ->type('votehead', 'Activity fees')
                ->press('Add Votehead')
                ->type('voteheadid', 'PE')
                ->type('votehead', 'Personnel emolument ')
                ->press('Add Votehead')
                ->type('voteheadid', 'M&I')
                ->type('votehead', 'Medical and Insurance')
                ->press('Add Votehead')
                ->pause(5000);
        });
    }
}
