<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class YearTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
//            $user = factory(User::class)->create();
            $browser
                ->maximize()
                ->loginAs(User::find(1))
//                ->pause(7000)
                ->visit('admin/year')
                ->type('year', '2017')
                ->press('Add Year')
                ->type('year', '2018')
                ->press('Add Year')
                ->type('year', '2019')
                ->press('Add Year');
        });
    }
}
