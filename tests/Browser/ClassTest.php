<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ClassTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/admin/form')
                ->maximize()
                ->select('form', '1')
                ->type('stream', 'B')
                ->press('Add Class')
                ->pause(1000)
                ->select('form', 'Form 1')
                ->type('stream', 'G')
                ->press('Add Class');
        });
    }
}
